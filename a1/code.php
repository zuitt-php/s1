<?php

//Address

function getFullAddress($country, $city, $province, $unit){
	return "$unit $city, $province, $country";
}

//Grading System

function getLetterGrade($grade){
	switch($grade){
		case ($grade >= 98):
			return "$grade is equivalent to A+.";
			break;
		case ($grade >= 95 && $grade <= 97):
			return "$grade is equivalent to A.";
			break;
		case ($grade >= 92 && $grade <= 94):
			return "$grade is equivalent to A-.";
			break;
		case ($grade >= 89 && $grade <= 91):
			return "$grade is equivalent to B+.";
			break;
		case ($grade >= 86 && $grade <= 88):
			return "$grade is equivalent to B.";
			break;
		case ($grade >= 83 && $grade <= 85):
			return "$grade is equivalent to B-.";
			break;
		case ($grade >= 80 && $grade <= 82):
			return "$grade is equivalent to C+.";
			break;
		case ($grade >= 77 && $grade <= 79):
			return "$grade is equivalent to C.";
			break;
		case ($grade >= 75 && $grade <= 76):
			return "$grade is equivalent to C-.";
			break;
		default:
			return "$grade is equivalent to D";
			break;
	}
}