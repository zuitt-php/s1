<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Activity 1</title>
</head>
<body>

<h1>Full Address</h1>
<p><?php echo getFullAddress('Philippines', 'Quezon City', 'Metro Manila', '#12 Notre Dame Street Brgy. Silangan');?></p>
<p><?php echo getFullAddress('Philippines', 'Bangar', 'La Union', 'San Fernando');?></p>

<h1>Letter-Based Grading</h1>
<p><?php echo getLetterGrade(99);?></p>
<p><?php echo getLetterGrade(96);?></p>
<p><?php echo getLetterGrade(93);?></p>
<p><?php echo getLetterGrade(90);?></p>
<p><?php echo getLetterGrade(87);?></p>
<p><?php echo getLetterGrade(84);?></p>
<p><?php echo getLetterGrade(81);?></p>
<p><?php echo getLetterGrade(78);?></p>
<p><?php echo getLetterGrade(76);?></p>
<p><?php echo getLetterGrade(72);?></p>
<p><?php echo getLetterGrade(50);?></p>

</body>
</html>